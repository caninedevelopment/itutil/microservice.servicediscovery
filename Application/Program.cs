﻿using ITUtil.Common.RabbitMQ.Common.Event;
using System;
using System.Collections.Generic;
using System.Text;

namespace Monosoft.Service.ServiceDiscovery
{
    class Program
    {
        static void Main(string[] args)
        {


            ITUtil.Common.Console.Program.StartRabbitMq(new System.Collections.Generic.List<ITUtil.Common.Command.INamespace>()
            {
                new Monosoft.Service.ServiceDiscovery.V1.ServiceDiscovery()
            },
            new List<IEventListener>
            {
                new Monosoft.Service.ServiceDiscovery.V1.EventLister()
            }
            );
        }
    }
}
