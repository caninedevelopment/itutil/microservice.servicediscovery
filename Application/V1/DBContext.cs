﻿namespace Monosoft.Service.ServiceDiscovery.V1
{
    using ITUtil.Common.Config;
    using ITUtil.Common.RabbitMQ.DTO;
    using ServiceStack.OrmLite;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Database
    /// </summary>
    public class DBContext
    {
        private static OrmLiteConnectionFactory dbFactory = null;
        private static string dbName = "servicediscovery";

        /// <summary>
        /// Initializes a new instance of the <see cref="Database"/> class.
        /// </summary>
        public DBContext(SQLSettings settings)
        {
            OrmLiteConnectionFactory masterDb = null;
            masterDb = GetConnectionFactory(settings);

            using (var db = masterDb.Open())
            {
                settings.CreateDatabaseIfNoExists(db, dbName);
            }

            // set the factory up to use the microservice database
            dbFactory = GetConnectionFactory(settings, dbName);
            using (var db = dbFactory.Open())
            {
                db.CreateTableIfNotExists<V1.Database.microservices>();
            }
        }

        private static OrmLiteConnectionFactory GetConnectionFactory(SQLSettings settings, string databasename = "")
        {
            switch (settings.ServerType)
            {
                case SQLSettings.SQLType.MSSQL:
                    return new OrmLiteConnectionFactory(settings.ConnectionString(databasename), SqlServerDialect.Provider);
                case SQLSettings.SQLType.POSTGRESQL:
                    return new OrmLiteConnectionFactory(settings.ConnectionString(databasename), PostgreSqlDialect.Provider);
                case SQLSettings.SQLType.MYSQL:
                    return new OrmLiteConnectionFactory(settings.ConnectionString(databasename), MySqlDialect.Provider);
                default:
                    return null;
            }
        }

        /// <summary>
        /// CreateOrUpdate
        /// </summary>
        /// <param name="servicesname">string servicesname</param>
        /// <param name="input">Monosoft.Gateway.DTO.ServiceDescriptor input</param>
        public void CreateOrUpdate(string servicesname, ServiceDescriptor input)
        {
            if (input == null)
            {
                throw new ArgumentNullException(nameof(input));
            }

            using (var db = dbFactory.Open())
            {
                var dbresult = db.Select<V1.Database.microservices>(p=>p.name == input.serviceName).FirstOrDefault();
                if (dbresult == null)
                {
                    db.Insert<V1.Database.microservices>(new V1.Database.microservices() { name=input.serviceName, json=input, lastseen = DateTime.Now.Ticks  });
                }
                else
                {
                    dbresult.json = input;
                    dbresult.lastseen = DateTime.Now.Ticks;
                    db.Update<V1.Database.microservices>(dbresult);
                }
            }
        }

        /// <summary>
        /// Cleanup
        /// </summary>
        public void Cleanup()
        {
            using (var db = dbFactory.Open())
            {
                var dbresult = db.Delete<V1.Database.microservices>(p => p.lastseen < DateTime.Now.AddMinutes(-5).Ticks);
            }
        }

        /// <summary>
        /// GetAll
        /// </summary>
        /// <returns>List ServicesInformation</returns>
        public List<ServiceDescriptor> GetAll()
        {
            
            using (var db = dbFactory.Open())
            {
                var dbresult = db.Select<V1.Database.microservices>().ToList();
                return dbresult.Select(p => p.json).ToList();
            }
        }

        public List<ServiceDescriptor> GetByName(string name)
        {
            using (var db = dbFactory.Open())
            {
                var dbresult = db.Select<V1.Database.microservices>().Where(p=> p.name == name).ToList();
                return dbresult.Select(p => p.json).ToList();
            }
        }
    }
}
