﻿using ITUtil.Common.Command;
using ITUtil.Common.RabbitMQ.DTO;
using Monosoft.Service.ServiceDiscovery.DTO;
using System.Collections.Generic;

namespace Monosoft.Service.ServiceDiscovery.V1
{
    public class getByService : GetCommand<getBySreviceDto, ServiceDescriptors>
    {
        public getByService() : base("Get information about a specifik service on the cluster")
        {

        }

        public override ServiceDescriptors Execute(getBySreviceDto input)
        {

            ServiceDescriptors res = new ServiceDescriptors() { Services = ServiceDiscovery.db.GetByName(input.ServiceName) };
            return res;
        }
    }
}