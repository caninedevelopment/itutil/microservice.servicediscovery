﻿namespace Monosoft.Service.ServiceDiscovery.V1
{
    using ITUtil.Common.Command;
    using ITUtil.Common.Config;
    using System.Collections.Generic;

    public class ServiceDiscovery : BaseNamespace
    {
        internal static DBContext db = new DBContext(GlobalRessources.getConfig().GetSetting<SQLSettings>());

        public ServiceDiscovery() : base("MicroServiceDiscovery", new ProgramVersion("1.0.0.1"))
        {
            commands.AddRange(new List<ICommandBase>() { new get(), new scan() });
        }
    }
}