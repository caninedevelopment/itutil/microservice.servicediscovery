﻿using ITUtil.Common.Command;
using ITUtil.Common.RabbitMQ.DTO;
using System.Collections.Generic;

namespace Monosoft.Service.ServiceDiscovery.V1
{
    public class get : GetCommand<object, ServiceDescriptors>
    {
        public get() : base("Get information about all services on the cluster")
        {

        }

        public override ServiceDescriptors Execute(object input)
        {
            ServiceDescriptors res = new ServiceDescriptors() { Services = ServiceDiscovery.db.GetAll() };
            return res;
        }
    }
}