﻿using ITUtil.Common.Base;
using ITUtil.Common.Command;
using ITUtil.Common.RabbitMQ.Message;
using ITUtil.Common.RabbitMQ.Request;
using System;
using System.Collections.Generic;

namespace Monosoft.Service.ServiceDiscovery.V1
{
    public class scan : UpdateCommand<object>
    {
        public scan() : base("Get information about all services on the cluster")
        {
            //this.claims.Add(
            //        new Claim()
            //        {
            //            key = "Monosoft.Service.MicroserviceDiscovery.systemadmin",
            //            dataContext = Claim.DataContextEnum.organisationClaims,
            //            description = new LocalizedString[]
            //            {
            //                new LocalizedString() { lang = "en", text = "system administrators are allowed to monitor the system services" },
            //            },
            //        }
            //    );
        }

        public override void Execute(object input)
        {
            string clientid = string.Empty;
            ServiceDiscovery.db.Cleanup();
            RequestClient.FAFInstance.FAF("servicediscovery.help", new RabbitMqHeader()
            {
                clientId = "N/A",
                Ip = "N/A",
                isDirectLink = false,
                messageId = "N/A",
                messageIssueDate = DateTime.Now,
                tokenInfo = null, /*TODO!*/
            },
            "{}",
            "");
        }
    }
}