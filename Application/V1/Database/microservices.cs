﻿using ITUtil.Common.RabbitMQ.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Monosoft.Service.ServiceDiscovery.V1.Database
{
    public class microservices
    {
        public string name { get; set; }
        public ServiceDescriptor json { get; set; } 
        public long lastseen { get; set; }
    }
}
