﻿using ITUtil.Common.RabbitMQ.DTO;

namespace Monosoft.Service.ServiceDiscovery.V1
{
    /// <summary>
    /// ServicesInformation
    /// </summary>
    public class ServicesInformation
    {
        /// <summary>
        /// Gets or sets Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets Description
        /// </summary>
        public ServiceDescriptor Description { get; set; }
    }
}
