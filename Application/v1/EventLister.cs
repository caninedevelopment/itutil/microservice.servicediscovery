﻿namespace Monosoft.Service.ServiceDiscovery.V1
{
    using ITUtil.Common.Config;
    using ITUtil.Common.RabbitMQ.Common.Event;
    using ITUtil.Common.RabbitMQ.DTO;
    using ITUtil.Common.RabbitMQ.Message;
    using static ITUtil.Common.RabbitMQ.Constants;

    /// <summary>
    /// EventLister_v1
    /// </summary>
    public class EventLister : IEventListener
    {
        private static DBContext db = new DBContext(GlobalRessources.getConfig().GetSetting<SQLSettings>());

        /// <inheritdoc/>
        public string ServiceName
        {
            get
            {
                return "servicediscovery";
            }
        }

        /// <inheritdoc/>
        public EventHandler Handler
        {
            get
            {
                return this.UpdateDatabase;
            }
        }

        /// <summary>
        /// UpdateDatabase
        /// </summary>
        /// <param name="topic">string[] topic</param>
        /// <param name="dto">ReturnMessageWrapper dto</param>
        public void UpdateDatabase(string[] topic, ReturnMessage dto)
        {
            if (dto == null)
            {
                throw new System.ArgumentNullException(nameof(dto));
            }
            var json = System.Text.Encoding.UTF8.GetString(dto.data);
            var msg = Newtonsoft.Json.JsonConvert.DeserializeObject<ServiceDescriptors>(json);

            foreach (var m in msg.Services)
            {
                if (string.IsNullOrEmpty(m.serviceName) == false)
                {
                    db.CreateOrUpdate(m.serviceName, m);
                }
            }
        }
    }
}