﻿//using System;
//using System.Collections.Generic;
//using System.Reflection;
//using Microsoft.OpenApi.Models;

//namespace Monosoft.Gateway.DTO
//{
//    /// <summary>
//    /// OperationDescription
//    /// </summary>
//    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:Element should begin with upper-case letter", Justification = "DTOs should always be camelCase")]
//    public class OperationDescription
//    {
//        /// <summary>
//        /// Convert
//        /// </summary>
//        /// <param name="instanceDescriptions">List ITUtil.Common.Utils.Bootstrapper.OperationDescription instanceDescriptions</param>
//        /// <returns>List OperationDescription</returns>
//        public static List<OperationDescription> Convert(List<ITUtil.Common.Utils.Bootstrapper.OperationDescription> instanceDescriptions)
//        {
//            if (instanceDescriptions == null)
//            {
//                throw new ArgumentNullException(nameof(instanceDescriptions));
//            }

//            List<OperationDescription> res = new List<OperationDescription>();
//            foreach (var instanceDescription in instanceDescriptions)
//            {
//                if (instanceDescription != null)
//                {
//                    res.Add(new OperationDescription(instanceDescription));
//                }
//            }

//            return res;
//        }

//        /// <summary>
//        /// Initializes a new instance of the <see cref="OperationDescription"/> class.
//        /// </summary>
//        /// <param name="instanceDescription">ITUtil.Common.Utils.Bootstrapper.OperationDescription instanceDescription</param>
//        public OperationDescription(ITUtil.Common.Utils.Bootstrapper.OperationDescription instanceDescription)
//        {
//            if (instanceDescription == null)
//            {
//                throw new ArgumentNullException(nameof(instanceDescription));
//            }

//            this.operation = instanceDescription.operation;
//            this.description = instanceDescription.description;
//            this.dataInputExample = this.ConvertTypeToOpenApi(instanceDescription.dataInputType);
//            this.dataOutputExample = this.ConvertTypeToOpenApi(instanceDescription.dataOutputType);
//            this.requiredClaims = instanceDescription.RequiredClaims;
//            if (this.requiredClaims == null)
//            {
//                this.requiredClaims = new ITUtil.Common.DTO.MetaDataDefinitions(Array.Empty<ITUtil.Common.DTO.MetaDataDefinition>());
//            }
//        }

//        /// <summary>
//        /// Initializes a new instance of the <see cref="OperationDescription"/> class.
//        /// </summary>
//        public OperationDescription()
//        {
//        }

//        private OpenApiSchema ConvertTypeToOpenApi(Type datatype)
//        {
//            OpenApiSchema schema = new OpenApiSchema();
//            if (datatype != null)
//            {
//                schema.Type = "object";
//                schema.Properties = new Dictionary<string, OpenApiSchema>();
//                foreach (var property in datatype.GetProperties())
//                {
//                    if (property.CanWrite && property.CanRead)
//                    {
//                        if (this.IsSimple(property.PropertyType))
//                        {
//                            schema.Properties.Add(
//                                property.Name,
//                                new OpenApiSchema()
//                                {
//                                    Type = this.GetPropertyJSType(property),
//                                    Description = string.Empty, // TODO!
//                                    Title = string.Empty, // TODO!

//                                    // Example = "", //TODO
//                                    Format = string.Empty, // TODO
//                                });
//                        }
//                        else
//                        { // TODO: hvordan skal vi håndtere cirkulær referencer?
//                            schema.Properties.Add(
//                                property.Name,
//                                this.ConvertTypeToOpenApi(property.PropertyType));
//                        }
//                    }
//                }

//                return schema;
//            }
//            else
//            {
//                return null;
//            }
//        }

//        private bool IsSimple(Type type)
//        {
//            var typeInfo = type.GetTypeInfo();
//            if (typeInfo.IsGenericType && typeInfo.GetGenericTypeDefinition() == typeof(Nullable<>))
//            {
//                // nullable type, check if the nested type is simple.
//                return this.IsSimple(typeInfo.GetGenericArguments()[0]);
//            }

//            return typeInfo.IsPrimitive
//              || typeInfo.IsEnum
//              || type.Equals(typeof(string))
//              || type.Equals(typeof(decimal));
//        }

//        private string GetPropertyJSType(PropertyInfo pi)
//        {
//            switch (pi.PropertyType.Name.ToLower(ITUtil.Common.Constants.Culture.DefaultCulture))
//            {
//                case "string":
//                    return "string";
//                case "int":
//                case "int16":
//                case "int32":
//                case "double":
//                case "decimal":
//                    return "number";
//                case "long":
//                case "int64":
//                    return "bigint";
//                case "bool":
//                case "boolean":
//                    return "boolean";
//                default:
//                    throw new Exception("Unknown datatype " + pi.PropertyType.Name + " for " + pi.Name);
//            }
//        }

//        /// <summary>
//        /// Gets or sets operation
//        /// </summary>
//        public string operation { get; set; } // ex. "Insert", "Update", "Delete", "Get" etc.

//        /// <summary>
//        /// Gets or sets description
//        /// </summary>
//        public string description { get; set; } // developer description

//        /// <summary>
//        /// Gets or sets dataInputExample
//        /// </summary>
//        public OpenApiSchema dataInputExample { get; set; } // json description

//        /// <summary>
//        /// Gets or sets dataOutputExample
//        /// </summary>
//        public OpenApiSchema dataOutputExample { get; set; } // json description

//        // public List<EventDescription> events { get; set; }

//        /// <summary>
//        /// Gets or sets requiredClaims
//        /// </summary>
//        public ITUtil.Common.DTO.MetaDataDefinitions requiredClaims { get; set; }
//    }
//}
