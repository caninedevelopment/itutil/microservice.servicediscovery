﻿//namespace Monosoft.Gateway.DTO
//{
//    using System.Collections.Generic;
//    using ITUtil.Common.DTO;
//    using ITUtil.Common.Utils.Bootstrapper;

//    /// <summary>
//    /// ServiceDescriptor
//    /// </summary>
//    [System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:Element should begin with upper-case letter", Justification = "DTOs should always be camelCase")]
//    public class ServiceDescriptor
//    {
//        /// <summary>
//        /// Initializes a new instance of the <see cref="ServiceDescriptor"/> class.
//        /// </summary>
//        public ServiceDescriptor()
//        {
//            this.operationDescriptions = new List<OperationDescription>();
//        }

//        /// <summary>
//        /// Initializes a new instance of the <see cref="ServiceDescriptor"/> class.
//        /// </summary>
//        /// <param name="version">ProgramVersion version</param>
//        /// <param name="operationDescription">OperationDescription operationDescription</param>
//        public ServiceDescriptor(ProgramVersion version, OperationDescription operationDescription)
//        {
//            this.version = version;
//            this.operationDescriptions = new List<OperationDescription>() { operationDescription };
//        }

//        /// <summary>
//        /// Initializes a new instance of the <see cref="ServiceDescriptor"/> class.
//        /// </summary>
//        /// <param name="operationDescription">OperationDescription operationDescription</param>
//        public ServiceDescriptor(OperationDescription operationDescription)
//        {
//            this.operationDescriptions = new List<OperationDescription>() { operationDescription };
//        }

//        /// <summary>
//        /// Gets or sets serviceName
//        /// </summary>
//        public string serviceName { get; set; }

//        /// <summary>
//        /// Gets or sets routeNamespaces
//        /// </summary>
//        public string routeNamespaces { get; set; }

//        /// <summary>
//        /// Gets or sets version
//        /// </summary>
//        public ProgramVersion version { get; set; }

//        /// <summary>
//        /// Gets or sets operationDescriptions
//        /// </summary>
//        public List<OperationDescription> operationDescriptions { get; set; }
//    }
//}
